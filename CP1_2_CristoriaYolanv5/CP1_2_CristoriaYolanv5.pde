//Minim, BeatDetect
// http://code.compartmental.net/tools/minim/quickstart/
// http://code.compartmental.net/minim/
// Geraadpleegd op 20 november 2016
// Muziek: The Egg(2005), Walking away,Duitsland: Great Stuff
//GmbH/Ministry of Sound Recordings GmbH

//KEYS TO USE A = bring back to default
//Z - E - R - T - UP - DOWN
import ddf.minim.*;
import ddf.minim.analysis.*;
Minim minim;
AudioPlayer song;
BeatDetect beat;

float [] pointSprayX = new float[720];  /*lijst x spiraal*/
float [] pointSprayY = new float[720];  /*lijst y spiraal*/
float [] pointSprayX2 = new float[720];  /*lijst x spiraal*/
float [] pointSprayY2 = new float[720];  /*lijst y spiraal*/
float c = 0;  /*kleur*/
float colorChange = 0.01;  /*kleurverandering*/
int frequency = 100; 
int size = 100;  /*grootte*/
int runningPoint = 0; /*optellend cijfer*/
int specialize = 0; /*specializing*/
int bass = 0; /*bass*/
int ghostBass = 0; /**/

void setup() {
  size(960, 540);
  colorMode(HSB, 1, 1, 1, 1);
  minim = new Minim(this);
  song = minim.loadFile("techno.mp3", 2048);
  song.play();
  beat = new BeatDetect();
}

void draw() {
  beat.detect(song.mix);
  background(0);
  runningPoint++;
  stickEffect(runningPoint);
  size ++;
  if ( size > 200 || size < 0){
    size = size * -1;
  }
  if ( beat.isOnset()) { 
    frequency -= 20;
    if (frequency % 200 == 0){
      frequency = 100;
    }
  }

  if (key == 'a') {
    specialize = 0;
    size = 100;
  }
  if (key == 'z') {
    stickEffect2(runningPoint);
  }
  if (key == 'e') {
    stickEffect3(runningPoint);
  }
  if (key == 'r') {
    stickEffect4(runningPoint);
  }
  if (key == 't') {
    stickEffect5(runningPoint);
  }
}

void stickEffect(int runningPoint) {
  for (int i = 360; i < pointSprayX.length; i++) { //waardes geven voor sterren
    float rad = radians(i);
    float cosSpiral = cos(rad);
    float sinSpiral = sin(rad);
    pointSprayX[i] = round(width/2 + cosSpiral * size / xDwaler1(runningPoint - i, frequency / 10, size));
    pointSprayY[i] = round(height/2 + sinSpiral * size);
    pointSprayX2[i] = round(width/2 + cosSpiral * size / xDwaler1(runningPoint - i, frequency / 10, size));
    pointSprayY2[i] = round(height/2 + sinSpiral * size);
    noFill();
    stroke(0.5, c, 1, 1);
    c = c + colorChange; 
    if (c > 0.5 || c < 0) {
      colorChange = colorChange * -1;
    }   
    line( pointSprayX[i], pointSprayY[i], pointSprayX2[i], pointSprayY2[i]);
    size += 0.5;
  }
}

void stickEffect2(int runningPoint) {
  for (int i = 360; i < pointSprayX.length; i++) { //waardes geven voor sterren
    float rad = radians(i);
    float cosSpiral = cos(rad);
    float sinSpiral = sin(rad);
    pointSprayX[i] = round(width/2 + cosSpiral  * size / xDwaler1(runningPoint - i, frequency / 10, size));
    pointSprayY[i] = round(height/2 + sinSpiral * size / xDwaler1(runningPoint - i, frequency / 10, size));
    pointSprayX2[i] = round(width/2 + cosSpiral  * size / xDwaler2(runningPoint - i, frequency / 10, size));
    pointSprayY2[i] = round(height/2 + sinSpiral * size / xDwaler2(runningPoint - i, frequency / 10, size));
    noFill();
    stroke(0.5, c, 1, 1);
    c = c + colorChange; 
    if (c > 0.5 || c < 0) {
      colorChange = colorChange * -1;
    }   
    line( pointSprayX[i], pointSprayY[i], pointSprayX2[i], pointSprayY2[i]);
    size += 1.5;
  }
}

void stickEffect3(int runningPoint) {
  for (int i = 360; i < pointSprayX.length; i++) { //waardes geven voor sterren
    float rad = radians(i - 90);
    float cosSpiral = cos(rad);
    float sinSpiral = sin(rad);
    pointSprayX[i] = round(width/2 + cosSpiral * size);
    pointSprayY[i] = round(height/2 + sinSpiral * size / sin(xDwaler1(runningPoint - i, frequency / 10, size)));
    noFill();
    stroke(0.5, c, 1, 1);
    c = c + colorChange; 
    if (c > 0.5 || c < 0) {
      colorChange = colorChange * -1;
    }   
    fill(1,0,1,0.2);
    point(pointSprayX[i], pointSprayY[i]);
    noFill();
  }
}

void stickEffect4(int runningPoint) {
  for (int i = 360; i < pointSprayX.length; i++) { //waardes geven voor sterren
    float rad = radians(i);
    float cosSpiral = cos(rad);
    float sinSpiral = sin(rad);
    pointSprayX[i] = round(width/2 + cosSpiral * size * xDwaler1(runningPoint - i, frequency / 10, size));
    pointSprayY[i] = round(height/2 + sinSpiral * size);
    pointSprayX2[i] = round(width/2 + cosSpiral * size * xDwaler1(runningPoint - i, frequency / 10, size));
    pointSprayY2[i] = round(height/2 + sinSpiral * size);
    noFill();
    stroke(0.5, c, 1, 1);
    c = c + colorChange; 
    if (c > 0.5 || c < 0) {
      colorChange = colorChange * -1;
    }   
    line( pointSprayX[i], pointSprayY[i], pointSprayX2[i], pointSprayY2[i]);
    size += 0.5;
  }
}

void stickEffect5(int runningPoint) {
  for (int i = 360; i < pointSprayX.length; i++) { //waardes geven voor sterren
    float rad = radians(i);
    float cosSpiral = cos(rad);
    float sinSpiral = sin(rad);
    pointSprayX[i] = round(width/2 + cosSpiral * size * xDwaler1(runningPoint - i, frequency / 10, size));
    pointSprayY[i] = round(height/2 + sinSpiral * size);
    pointSprayX2[i] = round(width/2 + cosSpiral * size * xDwaler1(runningPoint - i, frequency / 10, size));
    pointSprayY2[i] = round(height/2 + sinSpiral * size);
    noFill();
    stroke(0.5, c, 1, 1);
    c = c + colorChange; 
    if (c > 0.5 || c < 0) {
      colorChange = colorChange * -1;
    }   
    line( pointSprayX[i], pointSprayY[i], pointSprayX2[i], pointSprayY2[i]);
    size += 0.5;
  }
}


/*Swirling Points*/
float xDwaler1(float x, float frequency, float size) {
  return sin(x / frequency) *size + sin(x / frequency/100) *size;
}

float yDwaler1(float x, float frequency, float size, float specialize) {
  return cos(x / frequency/10) *size + sin(x / frequency) *size/10;
}

float xDwaler2(float x, float frequency, float size) {
  return sin(x / frequency/5) * size/2 + sin(x / frequency/100) *size;
}

float yDwaler2(float x, float frequency, float size, float specialize) {
  return cos(x / frequency/15) * size/2 + sin(x / frequency) *size/10;
}