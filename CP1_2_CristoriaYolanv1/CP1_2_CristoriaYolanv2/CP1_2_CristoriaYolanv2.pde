//Minim, BeatDetect
// http://code.compartmental.net/tools/minim/quickstart/
// http://code.compartmental.net/minim/
// Geraadpleegd op 20 november 2016
// Muziek: The Egg(2005), Walking away,Duitsland: Great Stuff
//GmbH/Ministry of Sound Recordings GmbH
import ddf.minim.*;
import ddf.minim.analysis.*;
Minim minim;
AudioPlayer song;
BeatDetect beat;

float runningPoint;
float c = 0;
float colorChange = 0.01;

float [] pointSpray = new float[100];


void setup() {
  size(960, 540);
  colorMode(HSB, 1, 1, 1, 1);
  minim = new Minim(this);
  song = minim.loadFile("techno.mp3", 2048);
  song.play();
  beat = new BeatDetect();
}
void draw() {
  background(0);
  fill(255, 63, 52);
  beat.detect(song.mix);

  stroke(c, 1, 1, 1);  //colorChanger
  c = c + colorChange; 
  if (c > 1 || c < 0) {
    colorChange = colorChange * -1;
  }

  strokeWeight(5);
  //line(width/2 + xDwaler1(runningPoint, 20), height/2 + yDwaler1(runningPoint, 20),width/2 + xDwaler2(runningPoint*2, 20), height/2 + yDwaler2(runningPoint*2, 20));
  for (int i = 0; i < 50; i++) {
    if (i % 3 == 0) {
      line(width/2 + xDwaler1(runningPoint - i, 20), height/2 + yDwaler1(runningPoint + i/15, 20), width/2 + xDwaler2(runningPoint + i*2, 20), height/2 + yDwaler2(runningPoint + i *2, 20));
    }
  }
  
  runningPoint++;
  if ( beat.isOnset()) {    
    if (keyPressed) {
      if (keyCode == 'a') {
        if (frameCount % 100 == 0) {
        }
      }
      if (keyCode == 'z') {
      }
      if (keyCode == 'e') {
      }
      if (keyCode == 'r') {
      }
    }
  }
}


float xDwaler1(float x, float frequency) {
  return sin(x / frequency) * 200 + sin(x / frequency*10) *20;
}

float yDwaler1(float x, float frequency) {
  return cos(x / frequency/100) * 200 + cos(x / frequency *2) *5;
}

float xDwaler2(float x, float frequency) {
  return sin(x / frequency) * 200 + sin(x / frequency*2) *20;
}

float yDwaler2(float x, float frequency) {
  return cos(x / frequency/20) * 200;
}