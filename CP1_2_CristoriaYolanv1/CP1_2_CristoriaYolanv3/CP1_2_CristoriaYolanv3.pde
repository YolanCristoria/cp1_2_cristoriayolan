//Minim, BeatDetect
// http://code.compartmental.net/tools/minim/quickstart/
// http://code.compartmental.net/minim/
// Geraadpleegd op 20 november 2016
// Muziek: The Egg(2005), Walking away,Duitsland: Great Stuff
//GmbH/Ministry of Sound Recordings GmbH
import ddf.minim.*;
import ddf.minim.analysis.*;
Minim minim;
AudioPlayer song;
BeatDetect beat;

float runningPoint;
float c = 0;
float colorChange = 0.01;
int size = 200;
float [] pointSprayX = new float[1000];
float [] pointSprayY = new float[1000];
int frequency = 20;
int frequency2 = frequency;
int specialize = 1;
int resizer = 0;
int ellipseSize = 20;
int[] xpos = new int[50]; 
int[] ypos = new int[50]; 
int bass = 0;

void setup() {
  size(960, 540);
  colorMode(HSB, 1, 1, 1, 1);
  minim = new Minim(this);
  song = minim.loadFile("techno.mp3", 2048);
  song.play();
  beat = new BeatDetect();
  for (int i = 0; i < pointSprayX.length; i++) {
    pointSprayX[i] = random(width);
    pointSprayY[i] = random(width);
  }
  for (int i = 0; i < xpos.length; i++ ) {
    xpos[i] = 0;
    ypos[i] = 0;
  }
}
void draw() {
  background(0);
  fill(1, 0, 1, 1);
  beat.detect(song.mix);

  stroke(0.5, c, 1, 0.6);  //colorChanger
  c = c + colorChange; 
  if (c > 0.5 || c < 0) {
    colorChange = colorChange * -1;
  }

  strokeWeight(5);
  mainEllement(1,1,1);
  /*Moving line*/
  /*Moving line END*/
  /*Points*/
  for (int i = 0; i < pointSprayX.length; i++) {
    if ( beat.isOnset()) {
      bass++;
      stroke(1, 0, 1, 1);
    }
    stroke(1, 0, 1, c);
    noFill();
    strokeWeight(2);
    point(pointSprayX[i] + bass * random(4), pointSprayY[i]);
    if (bass % 5 == 0){
      bass--;
    }
  }
  
  /*Points End*/

  runningPoint++;
  if (key == 'a') {
    frequency = 20;
    frequency2 = frequency;
    specialize = 1;
    resizer = 0;
  }
  if (key == 'e') {
    frequency2 = 20;
    mainEllementTriangle(1,1,1);
    resizer += 1;
  }
  if (key == 't') {
    mainEllementEllipse(1,1,50);
 
  }
  if (keyPressed) {
    if (key == 'z') {
      frequency = 5;
    }
    if (keyCode == UP) {
      size += 10;
    }
    if (keyCode == DOWN) {
      size -= 10;
    }
  }
  if (key == 'r') {
    specialize = 4;
  }
  if ( beat.isOnset()) {   //ONBEAT
    frameCount = 0;
    if (size <= 200) {
      size += 50;
    }
    if (frequency >=1) {
    }
    fill(0.5, 1, 0.6, 0.2);
    noStroke();
    ellipse(width/2, height/2, 50, 50);
  }
  /*Circle minsize*/
  if (size >= 50) {
    size -= 2;
  }
  /*Circle minsize END*/
}


float xDwaler1(float x, float frequency, float size, float frequency2) {
  return sin(x / frequency) * size + sin(x / frequency2) *20;
}

float yDwaler1(float x, float frequency, float size, float specialize) {
  return cos(x / frequency) * size + specialize * sin(x / frequency2) *20 ;
}

float xDwaler2(float x, float frequency, float size, float frequency2) {
  return sin(x / frequency) * size + sin(x / frequency2) *20;
}

float yDwaler2(float x, float frequency, float size, float specialize) {
  return specialize * cos(x / frequency) * size ;
}

float xDwaler3(float x, float frequency, float size, float frequency2) {
  return sin(x / frequency) * size;
}

float yDwaler3(float x, float frequency, float size, float specialize) {
  return specialize * cos(x / frequency) * size ;
}

void mainEllement(int a, int b, int d) {
  for (int i = 0; i < 50; i++) {
    if (i % 3 == 0) {      
      strokeWeight(2);
      stroke(0.5, c, 1, 0.6);
      line(width/2 + xDwaler1(runningPoint - i, frequency, size * resizer, frequency2), height/2 + yDwaler1(runningPoint + i, frequency, size * resizer, specialize), width/2 + xDwaler2(runningPoint + i, frequency, size, frequency2), height/2 + yDwaler2(runningPoint + i, frequency, size, specialize));
    }
  }
}

void mainEllementTriangle(int a, int b, int d) {
  for (int i = 0; i < 50; i++) {
    if (i % 3 == 0) {      
      strokeWeight(2);
      stroke(0.5, c, 1, c );
      triangle(width + xDwaler1(runningPoint - i, frequency, size * resizer, frequency2), 0 + yDwaler1(runningPoint + i, frequency, size * resizer, specialize), width/2 + xDwaler2(runningPoint + i, frequency, size, frequency2), 0 + yDwaler2(runningPoint + i, frequency, size, specialize), width/2, height/2);
    }
  }
}

void mainEllementEllipse(int a, int b, int d) {
  for (int i = 0; i < 50; i++) {
    if (i % 3 == 0) {      
      strokeWeight(2);
      stroke(0.5, c, 1, c );
      ellipse(width/2 + xDwaler2(runningPoint + i, frequency, size, frequency2), height/2 + yDwaler2(runningPoint + i, frequency, size, specialize), d, d);
    }
  }
}